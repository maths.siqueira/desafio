#Configuração das Respectivas Classes de Cadastro

import os
from flask import Flask, request, render_template, url_for, jsonify
from flask_restful import Api, Resource
import json

import config
from app import app, api, db
from app.models.bases import baseA_CadastroGeral, baseB_Financeiro, baseB_Dividas, baseC_Transacoes, baseC_Consultas

token = '611e1aa2d6f85e197daca9f06d668b55'

#Inserção Cadastro Geral
class RegisterGeneral(Resource):
    def post(self):

        token_api = request.json['token']
        if token_api == token:

            request_json = request.get_json()

            name = request_json['name']
            document =request_json['document']
            phone = request_json['phone']
            mobile_phone = request_json['mobile_phone']
            email = request_json['email']
            date_birth = request_json['date_birth']
            gender = request_json['gender']
            marital_status = request_json['marital_status']
            address_type = request_json['address']['address_type']
            street = request_json['address']['street']
            number = request_json['address']['number']
            complement = request_json['address']['complement']
            neighborhood = request_json['address']['neighborhood']
            birth_date = request_json['birth_date']

            verificaCadastro = baseA_CadastroGeral.query.filter_by(document=document).count()

            if verificaCadastro == 0:
                cadastroGeral = baseA_CadastroGeral(document, name, phone, mobile_phone, email, gender, marital_status, address_type, street, number, complement, neighborhood, birth_date)
                db.session.add(cadastroGeral)
                db.session.commit()

                return {"message": "data entered successfully"}

            else:
                return {'warning': 'document already registered'}

        else:
            return {'error': 'invalid token, check and try again'}


#Inserção Registro Financeiro
class RegisterFinancial(Resource):
    def post(self):
        
        token_api = request.json['token']
        if token_api == token:

                request_json = request.get_json()
                document = request_json['document'] 
                monthly_income = request_json['monthly_income']
                profession = request_json['profession']
                patrimony = request_json['patrimony']
                quantity_of_goods = request_json['quantity_of_goods']
                assets = request_json['assets']
                
                for qtdofgoods in range(int(quantity_of_goods)):
                
                    assets_type = assets[int(qtdofgoods)]['assets_type']
                    state = assets[int(qtdofgoods)]['state']
                    value = assets[int(qtdofgoods)]['value']

                    cadastroFinanceiro = baseB_Financeiro(document, monthly_income, profession, patrimony, assets_type, state, value)
                    db.session.add(cadastroFinanceiro)
                    db.session.commit()

                return {"message": "data entered successfully"}
                    
        else:
            return {'error': 'invalid token, check and try again'}
        

#Inserção Registro de Débitos
class RegisterDebt(Resource):
    def post(self):

        token_api = request.json['token']
        if token_api == token:

            request_json = request.get_json()
            document = request_json['document']
            amount_divided = request_json['amount_divided']
            debts_in_arrears = request_json['debts_in_arrears']
            total_amount_due = request_json['total_amount_due']
            debts = request_json['debt']
            
            for debtsinarrears in range(int(debts_in_arrears)):
            
                debt_type =  debts[int(debtsinarrears)]['debt_type']
                value = debts[int(debtsinarrears)]['value']
                status = debts[int(debtsinarrears)]['status']

                cadastrarDebts = baseB_Dividas(document, debt_type, value, status, total_amount_due)
                db.session.add(cadastrarDebts)
                db.session.commit()

            return {"message": "data entered successfully"}

        else:
            return {'error': 'invalid token, check and try again'}

#inserção Registro de Transações
class RegisterTransactions(Resource):
    def post(self):
        
        token_api = request.json['token']
        if token_api == token:

            request_json = request.get_json()
            document = request_json['document']
            quantity_items = request_json['quantity_items']
            items = request_json['items']

            for quantityitems in range(int(quantity_items)):
            
                item_type =  items[int(quantityitems)]['type']
                institution = items[int(quantityitems)]['institution']
                credit_card_flag = items[int(quantityitems)]['credit_card_flag']
                value = items[int(quantityitems)]['value']
                establishment = items[int(quantityitems)]['establishment']
                idtransaction = items[int(quantityitems)]['idtransaction']

                cadastroTransactions = baseC_Transacoes(document, item_type, institution, credit_card_flag, value, establishment, idtransaction)
                db.session.add(cadastroTransactions)
                db.session.commit()

            return {"message": "data entered successfully"}


        else:
            return {'error': 'invalid token, check and try again'}
