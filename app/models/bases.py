#Configuração das Classes referente a Estrutura de Banco de Dados

import os
from app import app, db

class baseA_CadastroGeral(db.Model):
    __tablename__ = 'baseA_CadastroGeral'

    document = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255)) 
    phone = db.Column(db.Integer) 
    mobile_phone = db.Column(db.Integer) 
    email = db.Column(db.String(150)) 
    gender = db.Column(db.String(30)) 
    marital_status = db.Column(db.String(100)) 
    address_type = db.Column(db.String(20)) 
    street = db.Column(db.String(255)) 
    number = db.Column(db.Integer) 
    complement = db.Column(db.String(200)) 
    neighborhood = db.Column(db.String(255))
    birth_date = db.Column(db.String(10))

    def __init__(self, document, name, phone, mobile_phone, email, gender, marital_status, address_type, street, number, complement, neighborhood, birth_date):
        self.document = document
        self.name = name
        self.phone = phone
        self.mobile_phone = mobile_phone
        self.email = email
        self.gender = gender
        self.marital_status = marital_status
        self.address_type = address_type
        self.street = street
        self.number = number
        self.complement = complement
        self.neighborhood = neighborhood
        self.birth_date = birth_date


class baseB_Financeiro(db.Model):
    __bind_key__ = 'ElasticSearch'
    __tablename__ = 'baseB_financeiro'
    
    id_financeiro = db.Column(db.Integer, primary_key=True)
    document = db.Column(db.Integer) 
    monthly_income = db.Column(db.Float) 
    profession = db.Column(db.String(255)) 
    patrimony = db.Column(db.Float)
    assets_type = db.Column(db.String(100))
    state = db.Column(db.String(100))
    value = db.Column(db.Float)

    def __init__(self, document, monthly_income, profession, patrimony, assets_type, state, value):
        self.document = document
        self.monthly_income = monthly_income
        self.profession = profession
        self.patrimony = patrimony
        self.assets_type = assets_type
        self.state = state
        self.value = value


class baseB_Dividas(db.Model):
    __bind_key__ = 'ElasticSearch'
    __tablename__ = 'baseB_Dividas'

    document = db.Column(db.Integer, primary_key=True) 
    debt_type = db.Column(db.String(100)) 
    value = db.Column(db.Float)
    status = db.Column(db.String(100))
    total_amount_due = db.Column(db.Float)

    def __init__(self, document, debt_type, value, status, total_amount_due):
        self.document = document
        self.debt_type = debt_type
        self.value = value
        self.status = status
        self.total_amount_due = total_amount_due


class baseC_Transacoes(db.Model):
    __bind_key__ = 'Redis'
    __tablename__ = 'baseC_Transacoes'

    id_transaction = db.Column(db.Integer, primary_key=True)
    document = db.Column(db.Integer) 
    item_type = db.Column(db.String(100))
    institution = db.Column(db.String(100))
    card_flag = db.Column(db.String(50))
    value = db.Column(db.Float)
    establishment = db.Column(db.String(200))
    idtransaction = db.Column(db.String(255))

    def __init__(self, document, item_type, institution, card_flag, value, establishment, idtransaction):
        self.document = document
        self.item_type = item_type
        self.institution = institution
        self.card_flag = card_flag
        self.value = value
        self.establishment = establishment
        self.idtransaction = idtransaction


class baseC_Consultas(db.Model):
    __bind_key__ = 'Redis'
    __tablename__ = 'baseC_Consultas'

    document = db.Column(db.Integer, primary_key=True)
    last_query = db.Column(db.String(10))
    institution = db.Column(db.String(50))

    def __init__(self, document, last_query, institution):
        self.document = document
        self.last_query = last_query
        self.institution = institution

        