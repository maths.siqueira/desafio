#Configuração das Respectivas Classes de Consulta

import os
from datetime import date, datetime
from flask import Flask, request, render_template, url_for, jsonify
from flask_restful import Api, Resource
from sqlalchemy import column, update, desc, asc
import json

import config
from app import app, api, db
from app.models.bases import baseA_CadastroGeral, baseB_Financeiro, baseB_Dividas, baseC_Transacoes, baseC_Consultas

token_api = '611e1aa2d6f85e197daca9f06d668b55'
today = date.today()

# Consulta Dados Geral (Nome, Documento, Endereço Completo, Débitos)
class dataAGeneralData(Resource):
    def get(self, document, token):
        if token_api == token:
            countBaseADocument = baseA_CadastroGeral.query.filter_by(document=document).count()
            if countBaseADocument == 1:
                consultaBaseACadastroGeral = baseA_CadastroGeral.query.filter_by(document=document).first()
        
                countBaseBDebt = baseB_Dividas.query.filter_by(document=document).count()
                if countBaseBDebt == 0:
                    listDebt = {}
                else:
                    consultaBaseBDebt = baseB_Dividas.query.filter_by(document=document).all()
                    listDebt = []
                    for debt in consultaBaseBDebt:
                        listDebt.append({'type': debt.debt_type, 'value': debt.value, 'status': debt.status})

                return {'document': consultaBaseACadastroGeral.document, 'name': consultaBaseACadastroGeral.name, 'address': [{'type': consultaBaseACadastroGeral.address_type, 'street': consultaBaseACadastroGeral.street, 'number': consultaBaseACadastroGeral.number, 'complement': consultaBaseACadastroGeral.complement, 'neighborhood': consultaBaseACadastroGeral.neighborhood}], 'debt': listDebt}
            
            else:
                return {'Error': 'document does not exist'}

        else:
            return {'error': 'invalid token, check and try again'}

# Consulta de Score (Nome, Documento, Idade, Sexo, Salário, Profissão, Patrimonio, Quantidade de Bens, Endereço Completo, Bens)
class dataBScoreData(Resource):
    def get(self, document, token):
        if token_api == token:

            countBaseBDocument = baseB_Financeiro.query.filter_by(document=document).count()

            if countBaseBDocument == 0:
                {'return': 'there is no division for this document'}
            else:
                consultaBaseACadastroGeral = baseA_CadastroGeral.query.filter_by(document=document).first()
                b_date = consultaBaseACadastroGeral.birth_date               
                age = datetime.strptime(b_date, '%Y-%m-%d')
                calculate_age = today.year - age.year - ((today.month, today.day) < (age.month, age.day))

                consultaBaseBFinanceiroProfission = baseB_Financeiro.query.filter_by(document=document).first()
                qtdOfGoods = baseB_Financeiro.query.filter_by(document=document).count()
                if qtdOfGoods == 0:
                    listAssets = {}
                else:
                    assetsConsult = baseB_Financeiro.query.filter_by(document=document).all()
                    listAssets = []
                    for assets in assetsConsult:
                        listAssets.append({'type': assets.assets_type, 'state': assets.state, 'value': assets.value})

                return {'age': calculate_age, 'assets': listAssets, 'address': [{'type': consultaBaseACadastroGeral.address_type, 'street': consultaBaseACadastroGeral.street, 'number': consultaBaseACadastroGeral.number, 'complement': consultaBaseACadastroGeral.complement, 'neighborhood': consultaBaseACadastroGeral.neighborhood}], 'profession': consultaBaseBFinanceiroProfission.profession}
                
        else:
            return {'error': 'invalid token, check and try again'}

# Consulta de Transações (Documento,Última Pesquisa, Instituição que Pesquisou, Movimentação Financeira, Última Transação com Utilização de Cartão de Crédito)
class dataCTransactionsData(Resource):
    def get(self, document, token, institution):
        if token_api == token:

            countBaseCDocument = baseC_Transacoes.query.filter_by(document=document).count()

            if countBaseCDocument == 0:
                return {'return': 'there are no financial transactions for this document'}
            else:

                lastQueryDocument = baseC_Consultas.query.filter_by(document=document).first()
                transacoesBaseC = baseC_Transacoes.query.filter_by(document=document).all()
                ultimaTransacaoCartaoCredito = baseC_Transacoes.query.filter_by(document=document, item_type='cartao credito').order_by(desc(baseC_Transacoes.id_transaction)).first()
                countMonvFinanceira = baseC_Transacoes.query.filter_by(document=document).count()

                if countMonvFinanceira == 0:
                    listMovFinanceira = {}
                else:
                    listMovFinanceira = []
                    for movFinanceira in transacoesBaseC:
                        listMovFinanceira.append({"type": movFinanceira.item_type, "home_institution": movFinanceira.institution, "destination_institution": movFinanceira.card_flag, "value": movFinanceira.value, "idtransaction": movFinanceira.idtransaction})

                countLastQuerydocument = baseC_Consultas.query.filter_by(document=document).count()

                if countLastQuerydocument == 0:
                    insertLastConsutDocument = baseC_Consultas(document, date.today(), institution)

                    db.session.add(insertLastConsutDocument)
                    db.session.commit()

                else:
                    updateLastConsult = baseC_Consultas.query.filter_by(document=document).first()
                    updateLastConsult.document = document
                    updateLastConsult.last_consult = date.today()
                    updateLastConsult.institution = institution

                    db.session.commit()

                return {"document": document, 'last_query': lastQueryDocument.last_query, 'institution': lastQueryDocument.institution, 'movement': [listMovFinanceira], 'last_credit_card_purchase': [{'type': ultimaTransacaoCartaoCredito.item_type, "institution": ultimaTransacaoCartaoCredito.institution, "credit_card_flag": ultimaTransacaoCartaoCredito.card_flag, "value": ultimaTransacaoCartaoCredito.value, "establishment": ultimaTransacaoCartaoCredito.establishment, "idtransaction": ultimaTransacaoCartaoCredito.idtransaction}]}

        else:
            return {'error': 'invalid token, check and try again'}

