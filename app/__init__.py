from multiprocessing import Manager
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api


app = Flask(__name__)
app.config.from_object('config')
api = Api(app)
db = SQLAlchemy(app)


from app.models import bases
from app.controller import routes

