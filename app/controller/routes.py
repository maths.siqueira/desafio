#Arquivo contendo as rotas necessárias para inserção e consultas

#Importando Bibliotecas/Modulos
import os
from flask import Flask, request, render_template, url_for, jsonify
from flask_restful import Api, Resource
import json

import config
from app import app, api, db
#from app.models.bases import baseA_CadastroGeral, baseB_Financeiro, BaseB_Dividas, baseC_Transacoes
from app.models.classesRegister import RegisterGeneral, RegisterFinancial, RegisterDebt, RegisterTransactions
from app.models.classesConsult import dataAGeneralData, dataBScoreData,dataCTransactionsData

#Rota principal
@app.route('/')
def home():
    return "Utilizar o seguinte Token: 611e1aa2d6f85e197daca9f06d668b55"


#Rotas contendo endereçamentos das APIS

## Cadastro
api.add_resource(RegisterGeneral, '/register/general')
api.add_resource(RegisterFinancial, '/register/financial')
api.add_resource(RegisterDebt, '/register/debt')
api.add_resource(RegisterTransactions, '/register/transactions')

## Consulta
api.add_resource(dataAGeneralData, '/dataA/generaldata/token=<token>&document=<document>')
api.add_resource(dataBScoreData, '/dataB/scoredata/token=<token>&document=<int:document>')
api.add_resource(dataCTransactionsData, '/dataC/transactionsdata/token=<token>&document=<int:document>&institution=<institution>')

