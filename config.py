from email.mime import base
#Arquivo contendo as configurações

import os
from app import app



basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = 'BATATINHA_FRITA_123'

#Instanciando Bancos
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'mysql.db')
app.config['SQLALCHEMY_BINDS'] = {
                                    'ElasticSearch': 'sqlite:///' + os.path.join(basedir, 'elasticsearch.db'),
                                    'Redis': 'sqlite:///' + os.path.join(basedir, 'redis.db')
                                 }


