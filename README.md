<a href="https://gitlab.com/maths.siqueira/desafio/-/blob/main/Documenta%C3%A7%C3%A3o.pdf"># Documentação</a>

## Desafio

Trabalhamos constantemente com grande volume e complexidade de dados. O Desafio é elaborar uma solução que ofereça armazenamento, processamento e disponibilização desses dados considerando boas práticas de segurança. Sendo que o principal ativo são dados sensíveis dos consumidores brasileiros.

# Cenário de Armazenamento

Supondo que existam três grandes bases de dados externas organizadas da seguinte forma.
- A primeira delas, que chamamos de Base A, é extremamente sensível e deve ser protegida com os maiores níveis de segurança, mas o acesso a esses dados não precisa ser tão performática. 
- A segunda, é a Base B que também possui dados críticos, mas ao contrário da Base A, o acesso precisa ser um pouco mais rápido. Uma outra característica da Base B é que além de consultas ela é utilizada para extração de dados por meio de algoritmos de aprendizado de máquina. 
- A última base, é a Base C, que não possui nenhum tipo de dado crítico, mas precisa de um acesso extremamente rápido.


# Tráfego

Cada uma das bases existentes, são acessadas por sistemas em duas diferentes arquiteturas: microserviços e nano-serviços. Ressaltando que essas bases de dados estão alocadas externamente.
O primeiro sistema, acessa os seguintes dados da Base A:
- CPF
- Nome
- Endereço
- Lista de dívidas

O segundo, acessa a Base B que contém dados para respctivos cálculo. O resultado destes cálculos é um rating utilizado por instituições de crédito (bancos, imobiliárias, etc) quando
precisam analisar o risco envolvido em uma operação de crédito a uma entidade.
- Idade
- Lista de bens (Imóveis, etc)
- Endereço
- Fonte de renda

O último serviço, acessa a Base C e tem como principal funcionalidade, rastrear eventos relacionados a um determinado CPF.
- Última consulta do CPF em um Bureau de crédito.
- Movimentação financeira nesse CPF.
- Dados relacionados a última compra com cartao de crédito vinculado ao CPF.

## Solução

### Sugestão de Arquitetura

![Screenshot](Fluxograma.jpg)

Sugestão:  Para a solução do problema a sugestão seria a criação de um Gateway ou Middleware na qual sua principal função seria executar a comunicação entre os micro e nanoserviços baseando-se em cada base de dados distinta tornando-se assim um método transparente para o item em questão. Baseando-se nisto tratamos com uma padronização na disponibilização dos dados tanto tem exbição quanto em inserção dos dados, ficando assim os micro e nanoserviços responsáveis pela integração via API com o Gateway ou Middleware discartando assim a responsabilidade de conexão direta com as bases de dados contidas em uma VPC AWS, garantindo desta forma que a única maneira de acesso aos dados seria por meio das referidas API.

### - Vantagens
- Bases de Dados não expostas diretamente para a Internet;
- Configuração de permissões de acessos entre somente as instâncias o host e o Gateway, ficando assim as bases de dados atrás de todo este cenário;
- Criação de um cache por um determinado tempo contendo consultas tornando assim uma segunda uma segunda requisição a estes dados mais rápiodo;
- Manter clientes conectados mesmo após a inserção ou descontinuação de serviços;
- Melhor configuração de monitoramentos sobre os Gateways (Zabbix por exemplo), verificando assim informações de quais APIs as pessoas estão utilizando de qual forma, Uptimes, dentre outras méticas futuros estudos de melhorias;
- Com a utilização de micro e nanoserviços pode-se efetuar chamadas a diversos serviços distintos;
- A única ponto de conexão entre o Front-end e o Back-end serão as APIs.

### - Desvantagens
- Curva de aprendizado sobre as tecnologias utilizadas;
- Revisão constante de aplicativos/estrutura mantendo assim a alta disponibilidade e performance;
- Monitoramento constante das APIs sobre possíveis pontos de falha e possíveis ataques.

### Proposta Tecnologias

- Gateway ou Middleware desenvolvido em Python 3.9
- Framework Frask 2.0.2
- Base de Dados A:
    - MariaDB:
        - Vantagens:
            - Níveis de segurança.
        - Desvantagens:
            - Baixo nível de performance. 
- Base de Dados B:
    - ElasticSerach:
        - Vantagens:
            - Velocidade e escalabilidade;
            - Capacidade de indexação mutiplos tipos de conteúdo como:
                - Busca em aplicações, websites, empresarial;
                - Logging e analitica de log´s;
                - Métricas de infraestrutura e montiramento de contaner, APM (Monitoramento de Performance de Aplicação);
                - Analitica de Segurança.
        - Desvantagens:
            - Necessário execução de consultas extras para obtenção de dados unidos em tempo de pesquisa.
- Base de Dados C:
    - Redis:
        - Vantagens:
            - Dados alocados na memória, portanto acesso as informações de forma mais rápida (acesso a dados de baixa latência e alta transferência), portanto reduz tempo de resposta em acessos externos;
            - Replicação e persistência;
            - Estrutura de dados flexíveis.
        - Desvantagens:
            - Ponto de atenção na escolha do hardware;
            - Armazenamento dos dados na memória volátil.
- Jenkins:
    - Garantia na entrega das aplicações (CI/CD - Continuous Integration/Continuous Delivery)
- AWS:
    - Garantia na disponibilidade dos serviços.
- GitLab:
    - Controle de versão do código;
    - Metricas de acompanhamento de qualidade de código;
- VPC:
    - Controle de Entrada e Saída de tráfego de sua rede e sub-rede
    - Flexibilização em regras de Firewall com controle de endereçamentos de IP -> Serviço
- HTTPS:
    - Comunicação criprografada
- JWT (JSON Web Token):
    - Transmissão e Armazenamento de forma segura e compacta dos objetos JSON.

### Estrutura de Desenvolvimento

- O Gateway/Middleware é elaborado em cima da arquitetura de software MVC (Model-View-Controller), onde:
    - Model:
        - Ponto de conexão entre as camadas de Visão (View) e Controle (Controller). Consiste na parte lógica da aplicação que gerencia todo comportamento dos dados por meio de regras de negócio, lógica e funções.
    - View:
        - Ponto na qual é exibido visualmente o resultado final.
    - Controller:
        - Ponto onde se faz a mediação da entrada e saída comandando assim o view e o model a serem alterados de forma apropriada conforme o usuário efetuou a solicitação;
        - Responsável pelo endereçamento e portas do servidor;
        - Efetuar todo controle de Rotas e realização de todas as regras de negócio.
No cenário pontual podemos classificar nosso cenário em dois pontos de codificação/informação/execução:
    - Primeiro Ponto:
        - Inclusão dos dados nas referidas Bases de Dados;
        - Processamento das informações nas referidas Bases de Dados.
        (Ambos pontos acima são gerenciados pelo Controller onde ocorre a validação tanto das rotas quanto dos dados baseados na referida regra de negócio, repasasndo assim para o Model que irá efetuar os referidos inserts nas respectivas bases)
    - Segundo Ponto:
        - Recebimento dos dados para consulta e execução dos referidos Select´s para obtenção de dados e retorno dos mesmos.

### Rotas/Endpoints

- Tipo: Entrada
    
Rota: api.servidor.com.br/register/general<br />
Autenticação: Token<br />
Header: Content-type -> application/json<br />
Método: POST<br />
Body Sample: <a href="https://gitlab.com/maths.siqueira/desafio#cadastro-geral">register/general</a><br />

Rota: api.servidor.com.br/register/financial<br />
Autenticação: Token<br />
Header: Content-type -> application/json<br />
Método: POST<br />
Body Sample: <a href="https://gitlab.com/maths.siqueira/desafio#cadastro-financeiro">register/financial</a><br />

Rota: api.servidor.com.br/register/debt<br />
Autenticação: Token<br />
Header: Content-type -> application/json<br />
Método: POST<br />
Body Sample: <a href="https://gitlab.com/maths.siqueira/desafio#cadastro-d%C3%ADvidas">register/debt</a><br />

Rota: api.servidor.com.br/register/transactions<br />
Autenticação: Token<br />
Header: Content-type -> application/json<br />
Método: POST<br />
Body Sample: <a href="https://gitlab.com/maths.siqueira/desafio#cadastro-transa%C3%A7%C3%B5es">register/transactions</a><br />

- Tipo: Saída

Rota: api.servidor.com.br/dataA/generaldata/token='token'&document='cpf'<br />
Autenticação: Token<br />
Header: Content-type -> application/json<br />
Método: GET<br />
Body Sample: <a href="https://gitlab.com/maths.siqueira/desafio#consulta-dados-gerais">dataA/generaldata</a><br />

Rota: api.servidor.com.br/dataB/scoredata/token='token'&document='cpf'<br />
Autenticação: Token<br />
Header: Content-type -> application/json<br />
Método: GET<br />
Body Sample: <a href="https://gitlab.com/maths.siqueira/desafio#consulta-score">dataB/scoredata</a><br />

Rota: api.servidor.com.br/dataC/transactionsdata/token='token'&document='cpf'<br />
Autenticação: Token<br />
Header: Content-type -> application/json<br />
Método: GET<br />
Body Sample: <a href="https://gitlab.com/maths.siqueira/desafio#consulta-transa%C3%A7%C3%B5es">dataC/transactionsdata</a><br />

### Body Sample

# Cadastro: Geral<br />
URL: api.servidor.com.br/register/general<br />
```json
{
    "token": "611e1aa2d6f85e197daca9f06d668b55",
    "name": "NOME COMPLETO",
    "document": "52411495030",
    "phone": "",
    "mobile_phone": "17981111111",
    "email": "email@email.com.br",
    "date_birth": "1981-12-01",
    "gender": "Masculino",
    "marital_status": "Casado",
    "address": 
        {
            "address_type": "Rua",
            "street": "Sebastião Alcantra",
            "number": "100",
            "complement": "Proximo Travessa 1",
            "neighborhood": "Jardim Vista Azul"
        }
}
```
# Cadastro: Financeiro<br />
URL: api.servidor.com.br/register/financial<br />
```json
{
    "token": "611e1aa2d6f85e197daca9f06d668b55",
    "document": "52411495030",
    "monthly_income": "10000.00",
    "profession": "Desenvolvedor Python",
    "patrimony": "450000.00",
    "quantity_of_goods": "2",
    "assets": [
        {
            "assets_type": "Imovel",
            "state": "Financiado",
            "value": "410000.00"
        },
        {
            "assets_type": "Automovel",
            "state": "Quitado",
            "value": "40000.00"
        }
    ]
}
```
# Cadastro: Dívidas<br />
URL: api.servidor.com.br/register/debt<br />
```json
{
    "token": "611e1aa2d6f85e197daca9f06d668b55",
    "document": "52411495030",
    "amount_divided": "1",
    "debts_in_arrears": "1",
    "total_amount_due": "10000.00",
    "debt": [
        {
            "type": "Financiamento",
            "value": "10000.00",
            "status": "Atrasado"
        }
    ]
}
```
# Cadastro: Transações<br />
URL: api.servidor.com.br/register/transactions<br />
```json
{
    "token": "611e1aa2d6f85e197daca9f06d668b55",
    "document": "52411495030",
    "quantity_items": "3",
    "items": [
        {
            "type": "cartao credito",
            "institution": "santander",
            "credit_card_flag": "MasterCard",
            "value": "1000.00",
            "establishment": "Rouparia Linha Fina",
            "idtransaction": "5"
        },
        {
            "type": "cartao debito",
            "institution": "santander",
            "credit_card_flag": "MasterCard",
            "value": "100.00",
            "establishment": "Auto Posto BR",
            "idtransaction": "435xc3srt34zt54qw4sz134r"
        },
        {
            "type": "cartao credito",
            "institution": "santander",
            "credit_card_flag": "MasterCard",
            "value": "50.00",
            "establishment": "Salgaderia Coxinha SoS",
            "idtransaction": "x34r13szrt1344r1s34at1r13tg313"
        }
    ]
}
```

# Consulta: Dados Gerais<br />
URL: api.servidor.com.br/dataA/generaldata/token=token&document=cpf<br />
```json
{
    "name": "OME COMPLETO",
    "document": "52411495030",
    "address": [
        {
            "address_type": "Rua",
            "street": "Sebastião Alcantra",
            "number": "100",
            "complement": "Proximo Travessa 1",
            "neighborhood": "Jardim Vista Azul"
        }
    ],
    "debt": [
        {
            "type": "Financiamento",
            "value": "10000.00",
            "status": "Atrasado"
        }
    ]
}
```
# Consulta: Score<br />
URL: api.servidor.com.br/dataB/scoredata/token=token&document=cpf<br />
```json
{
    "name": "NOME COMPLETO",
    "document": "52411495030",
    "age": "40",
    "gender": "Masculino",
    "monthly_income": "10000.00",
    "profession": "Desenvolvedor Python",
    "patrimony": "450000.00",
    "quantity_of_goods": "2",
    "address": [
        {
            "address_type": "Rua",
            "street": "Sebastião Alcantra",
            "number": "100",
            "complement": "Proximo Travessa 1",
            "neighborhood": "Jardim Vista Azul"
        }
    ],
    "assets": [
        {
            "type": "Imovel",
            "state": "Financiado",
            "value": "410000.00"
        },
        {
            "type": "Automovel",
            "state": "Quitado",
            "value": "40000.00"
        }
    ]
}
```
# Consulta: Transações<br />
URL: api.servidor.com.br/dataC/transactionsdata/token=token&document=cpf&institution='Bureau'<br />
```json
{
    "document": "52411495030",
    "last_query": "2022-01-20",
    "institution": "Serasa",
    "movement": [
        {
            "type": "Cartao Debito";
            "home_institution": "Santander",
            "destination_institution": "Banco do Brasil",
            "value": "10.00",
            "idtransaction": "32d1z2x4r4tzx3451t134t454x"

        },
         {
            "type": "Pagamento Boleto";
            "home_institution": "Santander",
            "destination_institution": "Bradesco",
            "value": "1452.12",
            "idtransaction": "f342s5s4tz35t2z5t2315t2s5z"

        },
         {
            "type": "PIX";
            "home_institution": "Santander",
            "destination_institution": "Nubank",
            "value": "5214.22",
            "idtransaction": "erw43r34x35t6d12y34s65s"

        }
    ],
    "last_credit_card_purchase": [
        {
            "type": "cartao credito",
            "institution": "santander",
            "credit_card_flag": "MasterCard",
            "value": "50.00",
            "establishment": "Salgaderia Coxinha SoS",
            "idtransaction": "x34r13szrt1344r1s34at1r13tg313"
        }
    ]
}
```

# Instalação/Execução

# Requisitos
    - Python 3.9.X

# Procedimento
    - Criar Virtualenv (python -m venv NOME_DA_PASTA)
    - Clonar repositório (git clone https://gitlab.com/maths.siqueira/desafio.git)
    - Ativação do Ambiente Virtual 
            - Acessar a pasta criada
            - Executar (Windows Scripts/Activate.ps1 ; Linux bin/activate)
    - Instalação das dependências: pip install -r requeriments.txt
    - Após instalação das dependências executar: python run.py

# Acesso
    - Modo Inserção das Informações:
        - Abrir <a href="https://insomnia.rest/download">Insomnia</a>
        - Na barra de URL inserir http://localhost:5000/'Payload desejado'/token=611e1aa2d6f85e197daca9f06d668b55

    - Modo Leitura das Informações:
        - Abrir <a href="https://insomnia.rest/download">Insomnia</a>
        - Na barra de URL inserir http://localhost:5000/'Payload desejado'/token=611e1aa2d6f85e197daca9f06d668b55?document=NUMERO_DOCUMENTO_CADASTRADO

